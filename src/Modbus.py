import uno
import unohelper
from net.kgmoney.libreoffice.Modbus import XModbus

import re
from pymodbus.client.sync import ModbusTcpClient

class ModbusImpl(unohelper.Base, XModbus):
	def __init__(self, ctx):
		self.ctx = ctx

	def ModbusRead(self, ipAddress, deviceId, mbAddress, trigger):
		p = re.compile('(\d+)(\d{4})(\.(\d+))?')
		m = p.match('0000' + mbAddress)
		v = -1

		client = ModbusTcpClient(ipAddress)

		if int(m.group(1)) == 0:
			v = client.read_coils(int(m.group(2)), 1, unit=deviceId).bits[0]
			v = int(v)

		elif int(m.group(1)) == 1 or int(m.group(1)) == 10:
			v = client.read_discrete_inputs(int(m.group(2)), 1, unit=deviceId).bits[0]
			v = int(v)

		elif int(m.group(1)) == 3 or int(m.group(1)) == 30:
			v = client.read_input_registers(int(m.group(2)), 1, unit=deviceId).registers[0]

			if m.group(4) != None:
				v = int((v & (1 << int(m.group(4)))) > 0)

		elif int(m.group(1)) == 4 or int(m.group(1)) == 40:
			v = client.read_holding_registers(int(m.group(2)), 1, unit=deviceId).registers[0]

			if m.group(4) != None:
				v = int((v & (1 << int(m.group(4)))) > 0)

		client.close()

		return v

	def ModbusWrite(self, ipAddress, deviceId, mbAddress, value):
		p = re.compile('(\d+)(\d{4})(\.(\d+))?')
		m = p.match('0000' + mbAddress)
		v = -1

		client = ModbusTcpClient(ipAddress)

		if int(m.group(1)) == 0:
			client.write_coil(int(m.group(2)), value > 0, unit=deviceId)
			v = client.read_coils(int(m.group(2)), 1, unit=deviceId).bits[0]
			v = int(v)

		elif int(m.group(1)) == 4 or int(m.group(1)) == 40:
			if m.group(4) != None:
				v = client.read_holding_registers(int(m.group(2)), 1, unit=deviceId).registers[0]

				if value > 0:
					v = v | (1 << int(m.group(4)))
				else:
					v = v & ~(1 << int(m.group(4)))

				value = v

			client.write_register(int(m.group(2)), value, unit=deviceId)
			v = client.read_holding_registers(int(m.group(2)), 1, unit=deviceId).registers[0]

			if m.group(4) != None:
				v = int((v & (1 << int(m.group(4)))) > 0)

		client.close()

		return v

def createInstance(ctx):
	return ModbusImpl(ctx)

g_ImplementationHelper = unohelper.ImplementationHelper()
g_ImplementationHelper.addImplementation(createInstance,"net.kgmoney.libreoffice.Modbus.python.ModbusImpl", ("com.sun.star.sheet.AddIn",),)
