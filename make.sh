PACKAGE_NAME=Modbus

LIBREOFFICE_BASE_PATH=/usr/lib/libreoffice
TOOLS_BIN_DIR=$LIBREOFFICE_BASE_PATH/sdk/bin
IDL_INCLUDE_DIR=$LIBREOFFICE_BASE_PATH/sdk/idl

# Compile IDL file.

IDL_FILE=idl/X$PACKAGE_NAME

"$TOOLS_BIN_DIR/idlc" -w -I "$IDL_INCLUDE_DIR" $IDL_FILE.idl

# Convert compiled IDL to loadable type library file.
# First remove existing .rdb file, otherwise regmerge will just
# append the compiled IDL to the resulting .rdb file. The joy of
# having an .rdb file with several conflicting versions of compiled
# IDL is very very limited - don't go there.

if [ -f $IDL_FILE.rdb ]; then
	rm $IDL_FILE.rdb
fi

"$LIBREOFFICE_BASE_PATH/ure/bin/regmerge" $IDL_FILE.rdb /UCR $IDL_FILE.urd

rm $IDL_FILE.urd


python src/generate_xml.py


#
# Create .OXT file
#
if [ -f $PACKAGE_NAME.oxt ]; then
	rm $PACKAGE_NAME.oxt
fi
if [ -d $PACKAGE_NAME ]; then
	rm -rf $PACKAGE_NAME
fi

mkdir -p $PACKAGE_NAME/META-INF


mv manifest.xml $PACKAGE_NAME/META-INF/
mv description.xml $PACKAGE_NAME/
mv CalcAddIn.xcu $PACKAGE_NAME/

mv $IDL_FILE.rdb $PACKAGE_NAME/
cp src/$PACKAGE_NAME.py $PACKAGE_NAME/

cd $PACKAGE_NAME/
zip -r ../$PACKAGE_NAME.oxt *

cd ..
rm -rf $PACKAGE_NAME
