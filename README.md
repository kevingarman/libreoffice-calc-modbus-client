# LibreOffice Calc Modbus Client #

### What is this repository for? ###

* This is an add-in for LibreOffice Calc that will allow you to read and write Modbus TCP registers from cells in a spreadsheet.

### How do I get set up? ###

* Simply download and double click the Modbus.oxt file and you will prompted to install the add-in
* This add-in requires LibreOffice, Python 3, and the pymobus module
